import React, { Component } from "react";
import { connect } from "react-redux";

class XucXac extends Component {
  // lấy data trên store về rồiduyệt mảng và render ra layout
  renderXucXac = () => {
    return this.props.mangXucXac.map((item, index) => {
      return (
        <img
          key={index}
          className="ml-2 mr-2"
          style={{ width: "50px" }}
          src={item.image}
          alt=""
        />
      );
    });
  };

  render() {
    return <div>{this.renderXucXac()}</div>;
  }
}

const mapStateToProps = (state) => {
  return {
    mangXucXac: state.XucXacReducer.mangXucXac,
  };
};

export default connect(mapStateToProps, null)(XucXac);
