import React, { Component } from "react";
import { connect } from "react-redux";

class KetQua extends Component {
  render() {
    return (
      <div>
        <div>
          Bạn Chọn:{" "}
          <span className="text-warning">
            {this.props.taiXiu ? "Tài" : "Xỉu"}
          </span>
        </div>
        <div>
          Số Bàn thắng:{" "}
          <span className="text-danger">{this.props.soBanThang}</span>
        </div>
        <div>
          Số bàn chơi:{" "}
          <span className="text-success">{this.props.soBanChoi}</span>
        </div>
      </div>
    );
  }
}

//lấy dữ liệu trên store về bằng...

const mapStateToProps = (state) => {
  return {
    // lấy data trên store về thành props của component
    taiXiu: state.XucXacReducer.taiXiu,
    soBanThang: state.XucXacReducer.soBanThang,
    soBanChoi: state.XucXacReducer.soBanChoi,
  };
};

//kết nối giữa store và component bằng thư viện connect
export default connect(mapStateToProps, null)(KetQua);
