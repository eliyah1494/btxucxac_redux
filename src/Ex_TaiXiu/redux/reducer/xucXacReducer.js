const initialState = {
  taiXiu: false, // true là tài,false là xỉu
  mangXucXac: [
    { value: 5, image: "./imgXucSac/5.png" },
    { value: 5, image: "./imgXucSac/5.png" },
    { value: 5, image: "./imgXucSac/5.png" },
  ],
  soBanThang: 0,
  soBanChoi: 0,
};

const XucXacReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LUA_CHON_TAI_XIU": {
      state.taiXiu = action.taiXiu;
      return { ...state };
    }

    case "PLAY_GAME": {
      //   cách 1
      //   let newMangXucXac = [];
      //   for (let i = 0; i < 3; i++) {
      //     let randomSoNgauNhien = Math.floor(Math.random() * 6) + 1;
      //     let itemMangXucXac = {
      //       value: randomSoNgauNhien,
      //       image: `./imgXucSac/${randomSoNgauNhien}.png`,
      //     };

      //     newMangXucXac.push(itemMangXucXac);
      //   }

      //   state.mangXucXac = newMangXucXac;
      //   return { ...state };

      //   cách 2
      let newMangXucXac = [...state.mangXucXac];
      newMangXucXac = newMangXucXac.map((item, index) => {
        let randomSoNgauNhien = Math.floor(Math.random() * 6) + 1;
        return {
          value: randomSoNgauNhien,
          image: `./imgXucSac/${randomSoNgauNhien}.png`,
        };
      });
      //   gán mảng xúc xác thành xúc xắc mới
      state.mangXucXac = newMangXucXac;
      //   xử lý số bàn chơi
      state.soBanChoi += 1;
      //   xử lý số bàn thắng
      //  1 tính tổng điểm dựa trên mảng xúc xắc mới;
      let tongSoDiem = newMangXucXac.reduce((tsd, item, index) => {
        return (tsd += item.value);
      }, 0);
      if (
        (tongSoDiem > 11 && state.taiXiu == true) ||
        (tongSoDiem <= 11 && state.taiXiu == false)
      ) {
        state.soBanThang += 1;
      }
      //   câp nhật lại dao diện
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default XucXacReducer;
