import { combineReducers } from "redux";
import XucXacReducer from "./xucXacReducer";

const rootXucXacReducer = combineReducers({
  XucXacReducer,
});

export default rootXucXacReducer;
