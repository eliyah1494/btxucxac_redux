import React, { Component } from "react";
import KetQua from "./KetQua";
import "./style.css";
import XucXac from "./XucXac";
import { connect } from "react-redux";
class Ex_TaiXiu extends Component {
  render() {
    return (
      <div className="game">
        <h3>Game xúc xắc</h3>
        <div className="container py-5">
          <div className="row">
            <div className="col-4">
              <button
                onClick={() => {
                  this.props.handkeLuaChonTaiXiu(true);
                }}
                className="btn btn-danger btn-game"
              >
                Tài
              </button>
            </div>

            <div className="col-4">
              <XucXac />
            </div>

            <div className="col-4">
              <button
                onClick={() => {
                  this.props.handkeLuaChonTaiXiu(false);
                }}
                className="btn btn-danger btn-game"
              >
                Xỉu
              </button>
            </div>
          </div>
          <div className="detail">
            <KetQua />
            <button onClick={this.props.playGame} className="btn btn-success">
              Play Game
            </button>
          </div>
        </div>
      </div>
    );
  }
}

// xử lí nút play game

const mapDispatchToProps = (dispatch) => {
  return {
    playGame: () => {
      const action = {
        type: "PLAY_GAME",
      };
      dispatch(action);
    },
    handkeLuaChonTaiXiu: (taiXiu) => {
      const action = {
        type: "LUA_CHON_TAI_XIU",
        taiXiu,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(Ex_TaiXiu);

// npm install redux ~ thư viện redux
// npm install react-redux ~ kết nối giữa react và redux
