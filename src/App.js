import logo from "./logo.svg";
import "./App.css";
import Ex_TaiXiu from "./Ex_TaiXiu/Ex_TaiXiu";

function App() {
  return (
    <div className="App">
      <Ex_TaiXiu />
    </div>
  );
}

export default App;
